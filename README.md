# Recursive Floyd's Algorithm

This code is a recursive implementation of Floyd's algorithm for finding the shortest path between all pairs of nodes in a graph. The purpose of the code is to reduce the execution time of the original version of Floyd's algorithm.


## Requirements

This code requires Python 3.7 installed on your machine.

## How to Run

The code can be run in any Python environment or console. Simply execute the script, and the program will run with the graph provided in the code.


## How it Works

The recursive function of Floyd's algorithm, floyd(), takes two parameters: a distance matrix and an intermediate node index. The distance matrix is a two-dimensional list representing the adjacency matrix of the graph, where NO _PATH represents a missing edge and 0 represents a self-loop.

The algorithm starts by initialising the kth intermediate node in the distance matrix and then iterates through all nodes to find the shortest path between each pair of nodes. If the intermediate node is identical to the start or end node, the function skips it.

Once the algorithm has traversed all intermediate nodes, the resulting distances are printed. The algorithm then continues with the next intermediate node and recursively calls the floyd() function until all intermediate nodes have been visited.

## Output

The output of the program is the resulting distance matrix, which represents the shortest path between all pairs of nodes in the input graph.


## Contributing

Contributions to this code are welcome. If you find any bugs or have suggestions for improvements, please open an issue or a pull request on the Gitlab repository.

## License

This code is licensed under the [MIT License](https://opensource.org/licenses/MIT).
