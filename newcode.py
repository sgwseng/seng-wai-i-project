import sys

NO_PATH = sys.maxsize
graph = [[0, 7, NO_PATH, 8],
         [NO_PATH, 0, 5, NO_PATH],
         [NO_PATH, NO_PATH, 0, 2],
         [NO_PATH, NO_PATH, NO_PATH, 0]]

def floyd(distance, k):
    """
    A simple implementation of Floyd's algorithm using recursion
    """
    n = len(distance)
    #Loop through all nodes and find the shortest path
    for i in range(n):
        for j in range(n):
            #if the nodes are the same, set the distance to 0
            if i == j:
                distance[i][j] = 0
                #If the intermediate node is the same as either start or end node, skip it
            elif i == k or j == k:
                continue
                #Otherwise, find the shortest path
            else:
                distance[i][j] = min(distance[i][j], distance[i][k] + distance[k][j])
    #If we've gone through all intermediate nodes, print the resulting distances
    if k == n - 1:
        # Any value that has sys.maxsize has no path
        print(distance)
        return
    #If not, continue with the next intermediate node
    floyd(distance, k + 1)

#Call the Floyd's algorithm function with the initial state of the graph
floyd(graph, 0)
